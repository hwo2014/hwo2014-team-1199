using System;

public class CarPositions {
	public CarPosition ownCarPosition {get; private set;}
	public CarPosition[] positions {get; private set;}

	public CarPositions(CarPosition[] positions, OurCarMsg ourCar)
	{
		foreach (CarPosition current in positions) {
			if (current.getColor() == ourCar.color) {
				this.ownCarPosition = current;
			}
		}
		this.positions = positions;
	}
}

public class CarPosition {
	public CarId id {get; private set;}
	public double angle {get; private set;}
	public PiecePosition piecePosition {get; private set;}

	public CarPosition(double angle, PiecePosition piecePosition, CarId id)
	{
		this.piecePosition = piecePosition;
		this.angle = angle;
		this.id = id;
	}

	public string getColor()
	{
		return this.id.color;
	}
}

public class PiecePosition
{
	public int pieceIndex {get; private set;}
	public double inPieceDistance {get; private set;}
	public Lane lane {get; private set;}
	public int lap {get; private set;}

	public PiecePosition(int pieceIndex, double inPieceDistance, Lane lane, int lap)
	{
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
		this.lap = lap;
	}
}

public class Lane
{
	public int startLaneIndex {get; private set;}
	public int endLaneIndex {get; private set;}

	public Lane(int startLaneIndex, int endLaneIndex)
	{
		this.startLaneIndex = startLaneIndex;
		this.endLaneIndex = endLaneIndex;
	}

	public bool isSwitching()
	{
		return this.startLaneIndex != this.endLaneIndex;
	}
}

public class CarId
{
	public string name {get; private set;}
	public string color {get; private set;}

	public CarId(string name, string color)
	{
		this.name = name;
		this.color = color;
	}
}