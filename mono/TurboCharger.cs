using System;

public class TurboCharger
{
	private TurboAvailability turboAvailable = null;
	private int turboAvailableUntilTick = 0;
	public bool goingTurbo {get; private set;}
	public bool hasTurbo {get; private set;}
	private Bot bot;

	public TurboCharger(Bot bot)
	{
		this.bot = bot;
	}

	public Thought tryCharge(int straightLength, int currentTick, double angle)
	{
		if( this.goingTurbo && currentTick > this.turboAvailableUntilTick ) {
			this.goingTurbo = false;
			this.bot.status = "Turbo off";
		}
		
		// If turbo isn't available or the track isn't a long straight, we shouldn't use it
		if (this.turboAvailable == null || straightLength < 4 || angle > 20)
		{
			return this.getThought(0.0);
		}

		// We can use turbo only once when it's available.
		this.turboAvailableUntilTick = (int)this.turboAvailable.turboDurationTicks + currentTick;
		this.turboAvailable = null;
		this.goingTurbo = true;
		this.hasTurbo = false;
		this.bot.status = "Going turbo";
		return this.getThought(2.0);
	}

	public void isCharged(TurboAvailability availabiliy)
	{
		this.turboAvailable = availabiliy;
		this.hasTurbo = (availabiliy != null);
	}

	private Thought getThought(double weight)
	{
		return new Thought(new TurboMsg(), weight);
	}
}

public class TurboMsg : SendMsg
{
	protected override Object MsgData() {
		return (Object) "Hyrsk hyrsk";
	}

	protected override string MsgType() { 
		return "turbo";
	}
}