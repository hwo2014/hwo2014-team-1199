using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Jarkka {

    private static int updateCount;
    private static double previousAngle = 0;
    private static int crashCount;
    private static string currentStatus;
    
    public Thought Ajaa(ReceivedMessageWrapper msg) {
        updateCount++;
        /*Console.WriteLine("--------------------------------");
        Console.WriteLine("-- Update count: "+updateCount);
        Console.WriteLine("--  Crash count: "+crashCount);*/
        
        
        JArray data = (JArray)msg.data; // Getting the data
        
        double currentThrottle = 1;
        double currentAngle = data.Value<JObject>(0).Value<double>("angle");
        
        if(currentAngle == 0 && previousAngle > 50){
            crashCount++;
        }
        
        previousAngle = currentAngle;
        
        if(Math.Abs(currentAngle) > 20){ // BEST 20 with 0.75
            currentThrottle = 0;
            currentStatus = "Slowing down!";
        }else{
            currentThrottle = 0.752;
            currentStatus = "Speeding up!";
        }
        
        if(Math.Abs(currentAngle) > 50) {
            return new Thought(
                new Throttle(0.0),
                0.35
            );
        }
        
        /*Console.WriteLine("--------------------------------");
        Console.WriteLine("    Status: " + currentStatus);
        Console.WriteLine("  Throttle: " + currentThrottle);
        Console.WriteLine(" Car Angle: " + currentAngle);
        Console.WriteLine("--------------------------------");*/
        
        return new Thought(
            new Throttle( currentThrottle ),
            0.0 // T�t� jos nostaa, niin yliajaa Jessen apukuskin :)
        );
    }
}