using System;

public class RacePosition {
    
    private static RacePosition _instance;
    public int carCount {get; private set;}
    public int currentPosition {get; private set;}
    
    
    public static RacePosition instance() {
        if( _instance == null ) {
            _instance = new RacePosition();
        }
        return _instance;
    }
    
    public void Update(CarPositions p) {
        
        this.currentPosition = 1;
        PiecePosition myCar = p.ownCarPosition.piecePosition;
        
        foreach (CarPosition carP in p.positions) {
            PiecePosition otherCar = carP.piecePosition;
            
            // Check lap
            if ( otherCar.lap > myCar.lap ) {
                this.currentPosition++;
            } else if( otherCar.lap == myCar.lap ) {
                
                // Check piece
                if( otherCar.pieceIndex > myCar.pieceIndex ) {
                    this.currentPosition++;
                } else if( otherCar.pieceIndex == myCar.pieceIndex ) {
                    
                    // Check in-piece distance
                    if( otherCar.inPieceDistance > myCar.inPieceDistance ) {
                        this.currentPosition++;
                    }
                }
            }
        }
        
    }
    
    public void Init(int carCount) {
        this.carCount = carCount;
    }
    
    public RacePosition() {
        
    }
}