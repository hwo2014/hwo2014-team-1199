public class OurCarMsg
{
	public string name;
	public string color;

	public OurCarMsg()
	{
		this.name = "";
		this.color = "";
	}

	public OurCarMsg(string name, string color)
	{
		this.name = name;
		this.color = color;
	}
}
