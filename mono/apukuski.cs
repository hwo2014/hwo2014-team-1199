using System;
using System.IO;
using System.Drawing;
using System.Net.Sockets;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Apukuski {
    
    // Boundaries for the statistics
    public static int w = 4000;
    public static int h = 12;
    public static int m = 100; // Margin
    public static int sh = 12; // Steps horizontal
    public static int sv = 25; // Steps vertical
    public static int scaleVertical = 1;
    public static int scale = 18;
    public static int scaleHorizontal = 80;
    
    
    // AI
    public static double predict		= 15;	 // How much shifted to left
    public static double minPredict		= -38;	 // How much shifted to left
    public static double maxSpeed 		= 12.0;  // Max speed for straights
    public static double curveMaxSpeed 		= 11.72;	 // Max speed for curve
    public static double gMultiplier 		= 0.69;  // The volume of hertz
    public static double kurviMultiplier	= 10.7;   // Effect of bad curve
    public static double gRecursion		= 0.40;  // How much prev g-force affects next one
    public static double minGRecursion		= 0.10;  // How much prev g-force affects next one
    public static double maxRadiusNormalized 	= 25.0; // Helper of 100% normalized radius
    public static double maxAngleNormalized	= 180.0;  // Helper of 100% normalized angle
    
    
    // Cache
    public TrackPiece[][] track;
    public PredictionPoint[][] targetSpeeds;
    public double lastCarPosition = 0;
    public int currentPieceIndex = 0;
    public int lastTickNumber = 0;
    public double currentSpeed = 0; // Pixels per tick
    public double currentCarPosition = 0;
    public double trackLength = 0;
    public int calculateNextSwithAtPieceIndex = 0;
    public int currentLane = 0;
    private string switchLaneTo = ""; // Use the function so this gets calculated
    
    // Debug
    private bool debug = false;
    private string imageName = "image";
    private Brush[] debugBrushes;
    Bitmap bmp;
    Graphics g;
    
    public static void Main(string[] args) {
        Console.WriteLine("Trolaolo");
        
	string[] filenames = {
	    "germany",
	    "usa",
	    "keimola",
	    "france",
	};
	
	foreach(string filename in filenames) {
	    
	    string json = System.IO.File.ReadAllText("jsons/" + filename + ".json");
	    JObject msg = JObject.Parse(json);
	    Apukuski ak = new Apukuski();
	    ak.HeraaPahviTassonKartta(msg.Value<JObject>("data"), true, "images/" + filename);
	}
    }
    
    public Apukuski() {
	// Alustetaan brushit (koska en tiennyt parempaakaan paikkaa tehd� t�t�)
	this.debugBrushes = new Brush[2];
	this.debugBrushes[0] = new SolidBrush(Color.DarkOrange);
	this.debugBrushes[1] = new SolidBrush(Color.PaleTurquoise);
    }

    public TrackPiece getCurrentTrackPiece()
    {
    	return this.track[this.currentLane][this.currentPieceIndex];
    }
    
    
    public void CarPositionsTick(CarPosition ownCar, int currentTick) {
	// Calculates the current speed of the car
	
	// Cachetetaan viimeisimm�t tiedot
	this.currentLane = ownCar.piecePosition.lane.endLaneIndex;
	this.currentLane = Math.Max(0,Math.Min(this.track.Length-1, this.currentLane));
	this.currentPieceIndex = ownCar.piecePosition.pieceIndex;
	this.currentPieceIndex = Math.Max(0,Math.Min(this.track[this.currentLane].Length-1, this.currentPieceIndex));
	
	// Tietoa nykyisest� paikasta
	double inPieceDistance = ownCar.piecePosition.inPieceDistance;
	TrackPiece currentPiece = this.track[this.currentLane][this.currentPieceIndex];
	this.currentCarPosition = (double)currentPiece.pieceStartPoint + inPieceDistance;
	
	// Lasketaan auton nykyinen nopeus
	int ticksSinceLastTick = currentTick - this.lastTickNumber;
	double pixesSinceLastTick = this.currentCarPosition - this.lastCarPosition;
	this.currentSpeed = pixesSinceLastTick / (double)ticksSinceLastTick;
	//Console.WriteLine("Current Speed: " + this.currentSpeed);
	
	int lap = Math.Max(0,ownCar.piecePosition.lap) % this.debugBrushes.Length;
	if(this.debug) {
	    // Piirret��n kivaa k�yr��
	    Brush brush = this.debugBrushes[lap];
	    DrawCircle(g, brush, P(this.currentCarPosition, this.currentSpeed), 1);
	    bmp.Save(this.imageName + ".png", System.Drawing.Imaging.ImageFormat.Png);
	}
	
	// Lis�� cachetusta
	this.lastCarPosition = currentCarPosition;
	this.lastTickNumber = currentTick;
    }
    
    
    public Thought SwitchLaneTo() {
	/**
	 *
	 * Haistelee, ett� milloin kannattaisi vaihtaa maisemaa (kaistaa).
	 * 1) T�m� tehd��n aina kun auto on switch-palalla + kerran aloituksessa
	 * 2) Lasketaan vasemman- ja oikeanpuoleisen kaistan pituus seuraavasta
	 *    switchist� sit� seuraavaan switchiin
	 * 3) Jos jompikumpi pituus on lyhyempi, niin vaihdetaan kaistaa
	 * 
	 **/
	
	if( this.lastTickNumber < 1 ) return null;
	
	// Check if need to recalcluate
	if( this.currentPieceIndex == this.calculateNextSwithAtPieceIndex ) {
	    
	    double shortestLength = 1000000000;
	    int shortestTrack = 0;
	    
	    
	    // Loop trough lanes +- 1
	    // (i = track)
	    for(int i = Math.Max(this.currentLane-1, 0); i <= Math.Min(this.currentLane+1, this.track.Length-1); i++) {
		
		int currPiece = this.currentPieceIndex + 1;
		double currLength = 0;
		TrackPiece trackPiece = track[i][currPiece];
		while( !trackPiece.hasSwitch ) {
		    currPiece = (currPiece + 1) % this.track[i].Length;
		    trackPiece = track[i][currPiece];
		}
		
		// Merkit��n seuraava switch-palanen seuraavaksi paikaksi ajaa
		// t�m� funkkari
		currPiece = (currPiece + 1) % this.track[i].Length;
		trackPiece = track[i][currPiece];
		this.calculateNextSwithAtPieceIndex = currPiece;
		
		// Haetaan kaikki palat siit� eteenp�in, seuraavaan switchiin
		// asti
		while( !trackPiece.hasSwitch ) {
		    currLength += trackPiece.length;
		    currPiece = (currPiece + 1) % this.track[i].Length;
		    trackPiece = track[i][currPiece];
		}
		
		
		if( currLength < shortestLength ) {
		    shortestTrack = i;
		    shortestLength = currLength;
		} else if( currLength == shortestLength ) {
		    // Suosi nykyist� linjaa
		    // (ei turhia kaistanvaihtoja)
		    if(i == this.currentLane) {
			shortestTrack = i;
		    }
		}
		
	    }
	    
	    
	    // Arvtaan seuraava kaista
	    if(shortestTrack == this.currentLane) {
		this.switchLaneTo = null;
		return null;
	    } else if(shortestTrack == this.currentLane-1) {
		this.switchLaneTo = "Left";
	    } else if(shortestTrack == this.currentLane+1) { 
		this.switchLaneTo = "Right";
	    }
	    
	    
	    //Console.WriteLine("Switching lane to: " + switchLaneTo);
	    return new Thought( new SwitchLane(this.switchLaneTo), 0.3 );
	}
	
	return null;
    }
    
    public double GetTargetCarSpeedForPosition(double carPosition, int currLane) {
	
	PredictionPoint lastPoint = this.targetSpeeds[currLane][0];
	foreach( PredictionPoint point in this.targetSpeeds[currLane] ) {
	    
	    // Meid�n pit�� hakea ensimm�inen pointti, joka on nykyist� pointtia
	    // edell�, niin voidaan verrata edelliseen pointtiin. Jepa? :)
	    if( point.startPoint > carPosition ) {
		
		// Ja t�t� distancea ei kakuteta, sill� k�ytet��n fluideja
		// PredictionPoint-objekteja, joiden paikka siis kerrotaan
		// aiemmalla vauhdilla.
		double distanceSinceLastKeyPoint = (double)(carPosition - lastPoint.startPoint);
		return (double)this.Lerp((double)lastPoint.speed, (double)point.speed, distanceSinceLastKeyPoint / (double)lastPoint.length);
	    }
	    
	    lastPoint = point;
	}
	
	// Viiminen pala t�ysii!
	return (double)maxSpeed;
    }
    
    public Thought HowHardToHitPedalToMedal() {
	double targetSpeed = this.GetTargetCarSpeedForPosition(this.currentCarPosition, this.currentLane);
	
	//Console.WriteLine("Current speed: " + this.currentSpeed);
	
	double targetThrottle = targetSpeed >= this.currentSpeed ? 1.0 : 0.0;
	
	return new Thought( new Throttle(targetThrottle), 0.2 );
    }
    
    public double Lerp(double from, double to, double percent) {
	percent = Math.Max(0,Math.Min(1,percent));
	return from + percent * ( to - from );
    }
    
    public void HeraaPahviTassonKartta(JObject kartta, bool debug = true, string imageName = "image") {
	this.debug = debug;
	this.imageName = imageName;
	
        int i = 0;
	    
	// Alustellaan debug-graafin kuvaa.
	// aw/ah = Actual Width/Height
	double aw = SH(w) + (m*2);
	double ah = SV(h) + (m*2);
	
	bmp = new Bitmap((int)aw,(int)ah);
	g = Graphics.FromImage(bmp);
	
	if( debug ) {
	    // Taustav�ri
	    g.FillRectangle(new SolidBrush(Color.White), 0,0,(int)aw,(int)ah);
	    //g.Dispose(); //???
	
	    // Draw base graph
	    Pen mainPen = new Pen(Color.FromArgb(100,100,100), 2);
	    Pen secPen = new Pen(Color.FromArgb(220,220,220), 1);
	    for(i = 0; i < sv; i++) {
		int c = w/sv*i; // Current
		g.DrawLine(i == 0 ? mainPen : secPen,P(c,0),P(c,h));
	    }
	    for(i = 0; i < sh; i++) {
		int c = h/sh*i; // Current
		g.DrawLine(i == 0 ? mainPen : secPen,P(0,c),P(w,c));
	    }
	}
	
	Pen GPen = new Pen(Color.Pink, 2); // Pen to simulate g-forces
	Pen FPen = new Pen(Color.Yellow, 2); // Pen to simulate forecast
	Pen[] SPen = {
	    new Pen(Color.Green, 2),
	    new Pen(Color.DarkGreen, 2),
	    new Pen(Color.ForestGreen, 2),
	    new Pen(Color.GreenYellow, 2)
	}; // Pen(s) to simulate speed
	Brush brush = new SolidBrush(Color.Red); // Brush to draw the piece cuts
	
	
	
	
	
	// Calculate tracks for each lane
	JArray lanes = kartta.Value<JObject>("race").Value<JObject>("track").Value<JArray>("lanes");
	this.track = new TrackPiece[lanes.Count][];
	this.targetSpeeds = new PredictionPoint[lanes.Count][];
	foreach( JObject lane in lanes.Children<JObject>() ) {
	    
	    i = 0;
	    double pieceStartPoint = 0;
	    
	    double lastG = 0;
	    Point lastGPoint = P(0,0);
	    Point newGPoint;
	    
	    Point lastFPoint = P(0,0);
	    Point newFPoint;
	    
	    Point lastSPoint = P(0,0);
	    Point newSPoint;
	    
	    double laneDiff = lane.Value<double>("distanceFromCenter");
	    int laneId = lane.Value<int>("index");
	    
	    // Let's loop trough all pieces of this puzzle
	    JArray pieces = kartta.Value<JObject>("race").Value<JObject>("track").Value<JArray>("pieces");
	    this.track[laneId] = new TrackPiece[pieces.Count];
	    this.targetSpeeds[laneId] = new PredictionPoint[pieces.Count];
	    foreach( JObject item in pieces.Children<JObject>() ) {
		
		bool hasSwitch = item.Value<bool>("switch");
		
		// Straight ones have length...
		double length = item.Value<double>("length");
		
		// ...but at curved ones there is not so we'll have to count it.
		// (in straight pieces, angle = 0)
		double absAbgle = 0;
		double kurviValue = 0;
		bool hasCurve = false;
		if(length == 0) {
		    
		    hasCurve = true;
		    double radius = item.Value<double>("radius");
		    double angle = item.Value<double>("angle");
		    absAbgle = Math.Abs(angle);
		    
		    bool kaantyyMyotapaivaan = (angle < 0);
		    double relativeLaneDiff = (kaantyyMyotapaivaan ? laneDiff : -laneDiff);
		    
		    // Lasketaan kurvin pituus, kaavalla:
		    // kurvinpituus = (r*2)*PI*(a/360)
		    double dLength = ((radius+relativeLaneDiff)*2) * Math.PI * (absAbgle/360);
		    
		    // Py�ristys
		    length = dLength;
		    
		    // Kurveista pit�� saada kurvikkaampia
		    // (sis�kurvi pahempi)
		    double normalizedAnlge = absAbgle / maxAngleNormalized;
		    double normalizedRadius = 1.0 / ( (radius+relativeLaneDiff) / maxRadiusNormalized );
		    kurviValue = ((normalizedAnlge + normalizedRadius) * kurviMultiplier);
		    //Console.WriteLine(kurviValue);
		}
		
		// Calculate G-forces
		double G = kurviValue + Math.Max(0, (lastG * gRecursion) - minGRecursion); // Simulated G-force
		
		// Calculate speed
		double S = (hasCurve ? curveMaxSpeed : maxSpeed)  - (G * gMultiplier);
		
		// Calculate prediction point
		double PrP = Math.Max(0,(pieceStartPoint-(predict * S)) - minPredict); // Prediction point
		
		// Draw g-forces
		newGPoint = P(pieceStartPoint,G);
		if( debug ) {
		    DrawCircle(g, brush, newGPoint, 2);
		    g.DrawLine(GPen, lastGPoint, newGPoint);
		}
		
		// Draw prediction
		newFPoint = P(PrP, G);
		if( debug ) {
		    //g.DrawLine(FPen, lastFPoint, newFPoint);
		}
		
		// Draw speed
		newSPoint = P(PrP, S);
		this.targetSpeeds[laneId][i] = new PredictionPoint(S, PrP, length);
		if( debug ) {
		    g.DrawLine(SPen[laneId], lastSPoint, newSPoint);
		}
		
		// Save the track piece
		this.track[laneId][i] = new TrackPiece(length, pieceStartPoint, absAbgle, G, hasSwitch);
		
		// Set variables for the next round
		i++;
		lastGPoint = newGPoint;
		lastG = G;
		lastFPoint = newFPoint;
		lastSPoint = newSPoint;
		
		// Next starting position
		pieceStartPoint = pieceStartPoint + length;
		this.trackLength = pieceStartPoint;
	    }
	}
	
	// Finally, save the debug data as image
	if( debug ) {
	    bmp.Save(imageName + ".png", System.Drawing.Imaging.ImageFormat.Png);
	    //bmp.Dispose();
	}
	
	Console.WriteLine("Kartta luettu. Lets menn��");
    }
    
    
    // Helper funkkarit hienoille graafeille (ett� voi venytt�� ja vanuttaa)
    private static Point P(double wp,double hp) {
	return new Point((int)SH(wp) + m, (int)SV(h-hp) + m);
    }
    
    private static double SV(double input) {
	return input / scaleVertical * scale;
    }
    
    private static double SH(double input) {
	return input / scaleHorizontal * scale;
    }
    
    private static void DrawCircle(Graphics g, Brush brush, Point point, int radius) {
	g.FillPie(brush, point.X, point.Y, radius*2, radius*2, 0, 360);
    }

    public int getCurrentStraightLength()
    {
    	int currentLength = 0;
    	int currentPieceIndex = (this.currentPieceIndex + 1) % this.track[this.currentLane].Length;
    	TrackPiece currentPiece = this.track[this.currentLane][currentPieceIndex];

    	while(currentPiece.isStraight()) {
    		currentLength++;
    		currentPieceIndex++;

    		// Track is out of bounds, we need to start from the beginning.
    		if (currentPieceIndex >= this.track[this.currentLane].Length)
    		{
    			currentPieceIndex = 0;
    		}

			currentPiece = this.track[this.currentLane][currentPieceIndex];
    	}

    	return currentLength;
    }
}

public class TrackPiece {
    public double length = 0;
    public double pieceStartPoint = 0;
    public double angle = 0;
    public double gforce = 0;
    public bool hasSwitch = false;
    
    public TrackPiece(double length, double pieceStartPoint, double angle, double gforce, bool hasSwitch) {
	this.length = length;
	this.pieceStartPoint = pieceStartPoint;
	this.angle  = angle;
	this.gforce = gforce;
	this.hasSwitch = hasSwitch;
    }

    public bool isStraight()
    {
    	return this.angle == 0;
    }
}

public class PredictionPoint {
    public double speed = 0;
    public double startPoint = 0;
    public double length = 0;
    
    public PredictionPoint(double speed, double startPoint, double length) {
	this.speed = speed;
	this.startPoint = startPoint;
	this.length = length;
    }
}