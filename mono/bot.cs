using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot {
	
	public static void Main(string[] args) {
		
		// These four are required for the CI
		string host = args[0];
		int port = int.Parse(args[1]);
		string botName = args[2];
		string botKey = args[3];
		SendMsg joinMessage = new Join(botName, botKey);
		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		
		// However, we can exploit this by additional parameters
		if( args.Length == 6 ) {
			string trackName = args[4];
			int carCount = int.Parse(args[5]);
			joinMessage = new JoinTrack(botName, botKey, trackName, carCount);
			Console.WriteLine("Track name: " + trackName + " with " + carCount + " cars");
		}


		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, joinMessage);
		}
	}

	private StreamWriter writer;
	private Apukuski apukuski;
	public string status = "Inited";
	private int lastGameTick = -1;
	private int lapsTotal = -1;

	Bot(StreamReader reader, StreamWriter writer, SendMsg join) {
		this.writer = writer;
		this.apukuski = new Apukuski();

		// This is just that the compiler wont crack it's pants... In reality we get our own car position before using it.
		OurCarMsg ourCar = new OurCarMsg();

		JObject response;
		string line;
		Jarkka jarkka = new Jarkka();
		RacePosition racePos = RacePosition.instance();
		TurboCharger turbo = new TurboCharger(this);
		int currTick = -1;

		send(join);

		while((line = reader.ReadLine()) != null) {
			response = JObject.Parse(line);

			ReceivedMessageWrapper msg = JsonConvert.DeserializeObject<ReceivedMessageWrapper>(line);

			switch( this.getMsgType(response) ) {
				case "carPositions":
					/**
					 * Here's how this works:
					 * 1) Run all methods that need to update
					 * 2) Run all methods that return Thought
					 * 3) Get the thought with biggest weight
					 * 4) Execute (send) that tought
					 */
					
					
					// Assign helper variables
					currTick = msg.gameTick;

					try {
						CarPositions positions = new CarPositions(this.getData(response).ToObject<CarPosition[]>(), ourCar);

						// 1) Run all "update" functions
						this.apukuski.CarPositionsTick(positions.ownCarPosition, currTick);
						racePos.Update(positions);
						this.Debug( this.apukuski, positions.ownCarPosition, currTick );
						
						// 2) Run thoughts to a list
						List<Thought> thoughts = new List<Thought>();
						
						thoughts.Add( this.idle(currTick) );
						thoughts.Add( this.apukuski.SwitchLaneTo() );
						thoughts.Add( this.apukuski.HowHardToHitPedalToMedal() );
						thoughts.Add( jarkka.Ajaa(msg) );
						thoughts.Add( turbo.tryCharge(apukuski.getCurrentStraightLength(), currTick, positions.ownCarPosition.angle) );
						
						
						// 3) Find the thought with most weight
						Thought strongestThought = null;
						foreach( Thought thought in thoughts ) {
							if( strongestThought == null || ( thought != null && strongestThought.weight < thought.weight ) ) {
								strongestThought = thought;
							}
						}
						
						/**
						 * Current weights:
						 * 
						 * | Method                    | Value |
						 * |---------------------------|-------|
						 * |�Jarkka ajaa               |�  0.0 |
						 * | Idle (normal)             |   0.1 |
						 * | PedalToMedal (apukuski)   |   0.2 |
						 * | SwitchLaneTo (apukuski)   |   0.3 |
						 * | Angle >50                 |  0.35 |
						 * | Idle (missed tick)        |   0.8 |
						 * |---------------------------|-------|
						 * 
						 */
	 					
						// 4) Execute
						send( strongestThought.action, currTick );
					} catch (JsonSerializationException e) {
						Console.WriteLine("Exception caused by: " + (string) response["data"]);
						throw e;
					}
					
					break;
					
				case "join":
					this.status = "Joined";
					send(new Ping());
					break;
				case "gameInit":
					this.status = "Race init";
					racePos.Init( ((JObject)msg.data).Value<JObject>("race").Value<JArray>("cars").Count );
					this.apukuski.HeraaPahviTassonKartta((JObject)msg.data, false);
					this.lapsTotal = ((JObject)msg.data).Value<JObject>("race").Value<JObject>("raceSession").Value<int>("laps");

					send(new Ping());
					break;
				case "gameEnd":
					this.status = "Race ended";
					send(new Ping());
					break;
				case "gameStart":
					this.status = "Race starts";
					send(new Ping());
					break;
				case "yourCar":
					ourCar = this.getData(response).ToObject<OurCarMsg>(); 
					break;
				case "turboAvailable":
					this.status = "Turbo available";
					turbo.isCharged(this.getData(response).ToObject<TurboAvailability>());
				break;
				default:
					if (currTick >= 0) {
						send(new Ping(), currTick);
					} else {
						send(new Ping());
					}
					break;
			}
		}
	}
	
	private Thought idle(int currentTick) {
		
		Thought returnThought;
		
		// Jos internet on hidas (ja tick tulee
		// liian my�h��n meille perille), ei
		// reagoida siihen mitenk��n.
		if( currentTick < this.lastGameTick ) {
			returnThought = new Thought(new Ping(), 0.8);
		} else {
			returnThought = new Thought(new Ping(), 0.1);
		}
		
		// Set cache for the next round
		this.lastGameTick = currentTick;
		
		return returnThought;
	}
	
	private bool writtenHeader = false;
	private void Debug(Apukuski apukuski, CarPosition ownCar, int currentTick) {
		
		if( this.writtenHeader == false ) {
			this.writtenHeader = true;
			Console.WriteLine("| Speed         | Angle     | Lap  | Position  | Lane  | Status               | Track position               |");
			Console.WriteLine("|---------------|-----------|------|-----------|-------|----------------------|------------------------------|");
		}
		
		int trackPos = (int)Math.Round((apukuski.currentCarPosition / apukuski.trackLength) * 30);
		int trackLeft = 30 - trackPos - 1;
		
		Console.Write("\r| "+
			"{0,5:##0.0}px/tick  | " +
			"{1,5:##0.0}deg  | "+
			"{2:0}/{3:0}  | "+
			"{4:0}       | "+
			"{5:0}     | "+
			"{6,-20} |"+
			"{7,"+trackPos+"}{8,"+trackLeft+"}|",
			apukuski.currentSpeed,
			ownCar.angle,
			ownCar.piecePosition.lap+1,
			this.lapsTotal,
			RacePosition.instance().currentPosition + "/" + RacePosition.instance().carCount,
			ownCar.piecePosition.lane.startLaneIndex,
			this.status,
			"=",""
		);
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}

	private void send(SendMsg msg, int currentTick) {
		writer.WriteLine(msg.ToJson(currentTick));
	}

	private string getMsgType(JObject json)
	{
		return (string) json["msgType"];
	}

	private JToken getData(JObject json)
	{
		return json["data"];
	}
}

public class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

public class MsgTickWrapper {
    public string msgType;
    public Object data;
    public int tick;

    public MsgTickWrapper(string msgType, Object data, int tick) {
    	this.msgType = msgType;
    	this.data = data;
    	this.tick = tick;
    }
}

public class ReceivedMessageWrapper {
    public string msgType;
    public Object data;
    public int gameTick;

    public ReceivedMessageWrapper(string msgType, Object data, int gameTick) {
    	this.msgType = msgType;
    	this.data = data;
    	this.gameTick = gameTick;
    }
}

public abstract class SendMsg {

	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}

	public string ToJson(int currentTick) {
		return JsonConvert.SerializeObject(new MsgTickWrapper(this.MsgType(), this.MsgData(), currentTick));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
	}

	protected override string MsgType() { 
		return "join";
	}
}
class JoinTrack: SendMsg {
	
	public Join botId;
	public string trackName;
	public int carCount;

	public JoinTrack(string name, string key, string trackName, int carCount) {
		this.botId = new Join(name, key);
		this.trackName = trackName;
		this.carCount = carCount;
	}

	protected override string MsgType() { 
		return "joinRace";
	}
}

public class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

public class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

public class SwitchLane : SendMsg {
	public string value;

	public SwitchLane(string value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "switchLane";
	}
}

public class Thought {
	public SendMsg action;
	public double weight; // 0.0 ... 1.0
	
	public Thought(SendMsg action, double weight) {
		this.action = action;
		this.weight = weight;
	}
}

public class TurboAvailability {
	public double turboDurationMilliseconds {get; private set;}
	public double turboDurationTicks {get; private set;}
	public double turboFactor {get; private set;}
}