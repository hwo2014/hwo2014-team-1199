module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      default: {
        files: 'mono/*.cs',
        tasks: ['shell:default'],
        options: {
          interrupt: true,
        },
      },
      apukuski: {
        files: 'mono/*.cs',
        tasks: ['shell:apukuski'],
        options: {
          interrupt: true,
        },
      }
    },
    shell: {
      default: {
        command: './clean && make',
        options: {
          stdout: true,
          stderr: true,
          execOptions: {
            cwd: 'mono/'
          }
        }
      },
      apukuski: {
        command: './clean && make apukuski && mono apukuski.exe',
        options: {
          stdout: true,
          stderr: true,
          execOptions: {
            cwd: 'mono/'
          }
        }
      }
    }
  });
  
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['shell:default', 'watch:default']);
  grunt.registerTask('apukuski', ['shell:apukuski', 'watch:apukuski']);
  grunt.registerTask('all', ['shell:apukuski', 'shell:default']);

};
